<?php

session_start();

require_once "models/User.php";
require_once "models/Article.php";

$latestArticles = Article::getLatest();

?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Awesome CMS</title>

    <link rel="shortcut icon" href="css/favicon.ico" type="image/x-icon">
    <link rel="icon" href="css/favicon.ico" type="image/x-icon">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body>

<?php
include "views/helper/navbar.php";
?>

<div class="jumbotron">
    <div class="container">
        <h1>Hello Awesome CMS!</h1>
        <p>This is a template for a simple marketing or informational website. It includes a large callout called a
            jumbotron and three supporting pieces of content. Use it as a starting point to create something more
            unique.</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
    </div>
</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row">

        <?php
        foreach ($latestArticles as $article) {
            echo '<div class="col-md-4">';
            echo '<h2>' . $article->getTitle() . '</h2>';
            echo '<p>' . limitArticleLengthToDisplay($article) . '</p>';
            echo '<p><a class="btn btn-default" ';
            echo 'href="views/article/fullView.php?id=' . $article->getId() . '"';
            echo ' role="button">Weiterlesen &raquo;</a></p>';
            echo '</div>';
        }
        ?>

    </div>

    <hr>

    <footer>
        <p>&copy; 2018 Broger, Gredler</p>
    </footer>
</div> <!-- /container -->

</body>
</html>