<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 05.01.2018
 * Time: 10:56
 */

require_once "DatabaseObject.php";

class Article implements DatabaseObject
{

    private $id;
    private $title, $content;
    private $date;
    private $owner;
    private $errors;


    /**
     * Article constructor.
     * @param $id
     * @param $title
     * @param $content
     * @param $date
     * @param $owner
     */
    public function __construct($id, $title, $content, $date, $owner)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->date = $date;
        $this->owner = $owner;
    }

    //#######################################################################
    //########### Custom Methods    #########################################
    //#######################################################################

    /**
     * Validate and save new article in database
     */
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                $this->update();
            } else {
                $this->id = $this->create();
            }
            return true;
        }
        return false;
    }


    /**
     * Validate article
     */
    public function validate()
    {
        return $this->validateHelper('Titel', 'title', $this->title, 50) &
            $this->validateHelper('Beitrag', 'content', $this->content, 15000) &
            $this->validateDate($this->date) &
            $this->validateOwner($this->owner);
    }


    /**
     * Helpermethod to validate length of input.
     * @param $label
     * @param $key
     * @param $value
     * @param $maxLength
     * @return bool
     */
    private function validateHelper($label, $key, $value, $maxLength)
    {
        if (strlen($value) == 0) {
            $this->errors[$key] = "$label darf nicht leer sein";
            return false;
        } else if (strlen($value) > $maxLength) {
            $this->errors[$key] = "$label zu lang (max. $maxLength Zeichen)";
            return false;
        } else {
            unset($this->errors[$key]);
            return true;
        }
    }

    /**
     * Confirm if date is today or in the future
     * @param $date
     * @return bool
     */
    private function validateDate($date)
    {
        if ($date >= date("Y-m-d", time())) {
            unset($this->errors['date']);
            return true;
        }
        $this->errors['date'] = "Freigabedatum darf nicht in der Vergangenheit liegen.";
        return false;
    }

    /**
     * Validate the articles owner.
     * @param $owner
     * @return bool
     */
    private function validateOwner($owner)
    {
        $allusers = User::getAll();
        foreach ($allusers as $u) {
            //When we compare objects using comparison operator ( ==),
            // two objects are equal if they are the instances of the same
            // class and have the same properties and values.
            if ($owner == $u) {
                unset($this->errors['user']);
                return true;
            }
        }
        $this->errors['owner'] = "Dieser Benutzer existiert nicht.";
        return false;
    }


    //#######################################################################
    //########### CRUD-Operations   #########################################
    //#######################################################################

    /**
     * Create a new article record in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO article (a_title, a_date, a_content, u_id) VALUES(?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->title, $this->date, $this->content, $this->owner->getId()));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    /**
     * Update an existing object in the database
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE article SET a_title = ?, a_date = ?, a_content = ?, u_id = ? WHERE a_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->title, $this->date, $this->content, $this->owner->getId(), $this->getId()));
        Database::disconnect();
    }

    /**
     * Get an article record from database
     * @param integer $id the id of the desired record
     * @return object Article object if the record with given id exists, else null
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM article WHERE a_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();

        if ($data == null) {
            return null;
        } else {
            return new Article($data["a_id"], $data["a_title"], $data["a_content"], $data["a_date"], User::get($data["u_id"]));
        }
    }

    /**
     * Get all article records from database
     * @return array array of Articles or an empty array
     */
    public static function getAll()
    {
        $articles = [];
        $db = Database::connect();
        $sql = "SELECT * FROM article";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        Database::disconnect();

        foreach ($data as $d) {
            $articles[] = new Article($d["a_id"], $d["a_title"], $d["a_content"], $d["a_date"], User::get($d["u_id"]));
        }
        return $articles;
    }

    /**
     * Get latest articles
     * @return array 3 latest articles
     */
    public static function getLatest()
    {
        $articles = [];
        $db = Database::connect();
        // SELECT latest 3 articles where the date is <= today
        $sql = "SELECT * FROM article WHERE a_date <= CURDATE() ORDER BY a_date DESC LIMIT 3";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        Database::disconnect();

        foreach ($data as $d) {
            $articles[] = new Article($d["a_id"], $d["a_title"], $d["a_content"], $d["a_date"], User::get($d["u_id"]));
        }
        return $articles;
    }

    public static function getByUserId($userId){
        $articles = [];
        $db = Database::connect();
        $sql = "SELECT * FROM article WHERE u_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($userId));
        $data = $stmt->fetchAll();
        Database::disconnect();

        foreach ($data as $d) {
            $articles[] = new Article($d["a_id"], $d["a_title"], $d["a_content"], $d["a_date"], User::get($d["u_id"]));
        }
        return $articles;
    }

    /**
     * Delete a record from the database
     * @param integer $id the id of the record to delete
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM article WHERE a_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

    //#######################################################################
    //########### Getters & Setters  ########################################
    //#######################################################################



    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }
}