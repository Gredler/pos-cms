<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 05.01.2018
 * Time: 10:56
 */

require_once "DatabaseObject.php";

class User implements DatabaseObject
{

    private $id;
    private $name, $password, $email;
    private $errors;

    /**
     * User constructor.
     * @param $id
     * @param $name
     * @param $password
     * @param $email
     */
    public function __construct($id, $name, $password, $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->password = $password;
        $this->email = $email;
    }

    //#######################################################################
    //########### Custom Methods    #########################################
    //#######################################################################

    /**
     * Check Username & Password combination
     */
    public static function login($username, $password)
    {
        $allUsers = User::getAll();
        foreach ($allUsers as $user) {
            if ($user->getName() === $username) {
                if (password_verify($password, $user->getPassword())) {
                    $_SESSION["user"] = $user->getId();
                    return true;
                } else {
                    $_SESSION['loginError'] = "Das Passwort ist falsch, bitte versuchen Sie es erneut!";
                    return false;
                }
            }
        }
        $_SESSION['loginError'] = "Dieser Username existiert nicht - um sich anzumelden müssen Sie registriert sein!";
        return false;
    }

    public static function logout()
    {
        unset($_SESSION["user"]);
    }

    /**
     * Validate user
     */
    public function validate()
    {
        return $this->validateHelper('Benutzername', 'uname', $this->name, 50) &
            $this->validateHelper('E-Mail', 'email', $this->email, 100);
    }

    /**
     * Helper for validating title and content.
     * @param $label
     * @param $key
     * @param $value
     * @param $maxLength
     * @return bool
     */
    private function validateHelper($label, $key, $value, $maxLength)
    {
        if (strlen($value) == 0) {
            $this->errors[$key] = "$label darf nicht leer sein";
            return false;
        } else if (strlen($value) > $maxLength) {
            $this->errors[$key] = "$label zu lang (max. $maxLength Zeichen)";
            return false;
        } else {
            unset($this->errors[$key]);
            return true;
        }
    }

    /**
     * Validate and save new user in database
     */
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                $this->update();
            } else {
                $this->id = $this->create();
            }
            return true;
        }
        return false;
    }

    /**
     * Check if new password and confirmation of new password are equal
     * @param $pw1 new password
     * @param $pw2 confirm new password
     * @return bool
     */
    public function confirmPassword($pw1, $pw2)
    {
        if ($pw1 != $pw2) {
            $this->errors['pwconfirm'] = "Passwörter müssen übereinstimmen!";
            return false;
        } else if (strlen($pw1) < 6) {
            $this->errors['pwconfirm'] = "Passwort muss mindestens 6 Zeichen lang sein.";
            return false;
        } else {
            unset($this->errors['pwconfirm']);
            return true;
        }
    }

    /**
     * Check, if password corresponds to user
     * @param $pwInput password to verify
     * @return bool
     */
    public function checkPassword($pwInput)
    {
        if (password_verify($pwInput, $this->getPassword())) {
            unset($this->errors['password']);
            return true;
        } else {
            $this->errors['password'] = "Passwort ist falsch.";
            return false;
        }
    }

    //#######################################################################
    //########### CRUD-Operations   #########################################
    //#######################################################################

    /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO user (u_name, u_password, u_email) VALUES(?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->password, $this->email));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE user SET u_name = ?, u_password = ?, u_email = ? WHERE u_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->password, $this->email, $this->id));
        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM user WHERE u_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();

        if ($data == null) {
            return null;
        } else {
            return new User($data["u_id"], $data["u_name"], $data["u_password"], $data["u_email"]);
        }
    }

    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll()
    {
        $users = [];
        $db = Database::connect();
        $sql = "SELECT * FROM user";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        Database::disconnect();

        foreach ($data as $d) {
            $users[] = new User($d["u_id"], $d["u_name"], $d["u_password"], $d["u_email"]);
        }
        return $users;
    }

    /**
     * Delete the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM user WHERE u_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();

        //if a user deletes his profile he will be logged out automatically
        User::logout();
        header("Location: index.php");
        exit();
    }

    //#######################################################################
    //########### Getters & Setters #########################################
    //#######################################################################

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }
}