<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 05.01.2018
 * Time: 15:41
 */

session_start();
if (!isset($_SESSION["user"])) {
    header("Location: ../../index.php");
    exit();
}
?>