<?php

function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

$path = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_FILENAME);


include "login.php";
require_once "func.php";

if (isset($_POST["logout"])) {
    User::logout();
}
?>

<script>
    // Get the modal
    var modal = document.getElementById('id01');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    function showLoginForm() {
        document.getElementById('id01').style.display = 'block'
    }

    function logout() {

    }
    <?php
    if (isset($_SESSION['loginError'])) {
        echo 'showLoginForm();';

        unset($_SESSION['loginError']);
    }
    ?>
</script>


<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/pos-cms/index.php">Awesome CMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <?php
                if (isset($_SESSION["user"])) {
                    showUserNavigation();
                }
                ?>
            </ul>

            <?php
            loginButtonManager();
            ?>

        </div><!--/.navbar-collapse -->
    </div>
</nav>
<br><br><br>