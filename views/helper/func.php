<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 05.01.2018
 * Time: 15:29
 */

/**
 * Show links for Users and Articles
 */
function showUserNavigation()
{
    echo '<li><a href="/pos-cms/views/article/index.php">Beiträge</a></li>';
    echo '<li><a href="/pos-cms/views/user/index.php">Benutzer</a></li>';
}

/**
 * Show login or logout button depending on whether a user is logged in or not
 */
function loginButtonManager()
{
    if (!isset($_SESSION["user"])) {
        echo '<div class="navbar-form navbar-right">';
        echo '<button onclick="showLoginForm();" type="submit" class="btn btn-success">Login</button>';
        echo '</div>';
    } else {
        echo '<form class="navbar-form navbar-right" action="/pos-cms/index.php" method="post">';
        echo '<button name="logout" type="submit" class="btn btn-warning">Logout</button>';
        echo '</form>';
    }
}

/**
 * Create a substring of an article
 * @param $article
 * @return string the full article if it has less than 500 chars, otherwise the first 496 chars of the article followed
 * by '...'
 */
function limitArticleLengthToDisplay($article){
    if (strlen($article->getContent()) < 500) {
        return $article->getContent();
    } else {
        return substr($article->getContent(), 0, 496) . '...';
    }
}

/**
 * get all Articles that should be displayed
 * @return array
 */
function getAllArticlesToDisplay(){
    if (isset($_GET['u_id'])) {
        if (User::get($_GET['u_id']) != null) {
            $allArticles = Article::getByUserId($_GET['u_id']);
        } else {
            $allArticles = Article::getAll();
        }
    } else {
        $allArticles = Article::getAll();
    }
    return $allArticles;
}

?>