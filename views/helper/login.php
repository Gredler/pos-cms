<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 05.01.2018
 * Time: 13:06
 */

if (isset($_POST["submit"])) {
    $result = User::login($_POST["uname"], $_POST["psw"]);
}

?>

<div id="id01" class="modal">

    <form class="modal-content animate" action="index.php" method="post">
        <div class="imgcontainer">
            <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
            <img src="/pos-cms/img/index.png" alt="Avatar" class="avatar" style="width: 150px; height: auto">
        </div>

        <?php
        if (isset($_SESSION["loginError"])) {
            echo '<div class="error">';
            echo $_SESSION["loginError"];
            echo '</div>';
        }

        ?>

        <div class="login-container">
            <label><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="uname" required>

            <label><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="psw" required>

            <button type="submit" name="submit">Login</button>
            <input type="checkbox" checked="checked"> Remember me
        </div>

        <div class="login-container login-cancel-area">
            <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">
                Cancel
            </button>
            <span class="psw">Forgot <a href="#">password?</a></span>
        </div>
    </form>
</div>
