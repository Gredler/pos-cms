<?php

include "../helper/sessionHelper.php";
include "../helper/head.php";
include "../../models/Article.php";
include "../../models/User.php";
include "../helper/navbar.php";

?>

<!DOCTYPE html>
<html lang="de">

<?php

$article = Article::get($_GET['id']);
if ($article == null) {
    header("Location: index.php");
    exit();
}

if (isset($_POST['update'])) {
    $article->setTitle($_POST['title']);
    $article->setContent($_POST['content']);
    $article->setDate($_POST['releasedate']);
    $article->setOwner(User::get($_POST['owner']));

    if ($article->validate()) {
        $article->save();
        header("Location: view.php?id=" . $article->getId());
        exit();
    }
}
?>

<body>

<div class="container">
    <div class="row">
        <h2>Beitrag bearbeiten</h2>
    </div>

    <form class="form-horizontal" action="update.php?id=<?= $_GET['id'] ?>" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required">
                    <label class="control-label">Titel *</label>
                    <input type="text" class="form-control" name="title" maxlength="45"
                           value="<?= htmlspecialchars($article->getTitle())  ?>">
                    <?php
                    if (isset($article->getErrors()['title'])) {
                        echo '<p class="error">' . $article->getErrors()['title'] . '</p>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required">
                    <label class="control-label">Freigabedatum *</label>
                    <input type="date" class="form-control" name="releasedate"
                           value="<?= htmlspecialchars($article->getDate()) ?>">
                    <?php
                    if (isset($article->getErrors()['date'])) {
                        echo '<p class="error">' . $article->getErrors()['date'] . '</p>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="form-group required">
                    <label class="control-label">Besitzer *</label>
                    <select class="form-control" name="owner" <?php
                    if ($_SESSION['user'] != $article->getOwner()->getId()) {
                        echo "disabled";
                    }
                    ?>>
                        <option value="" disabled>-Besitzer auswählen-</option>
                        <?php
                        $allUsers = User::getAll();
                        foreach ($allUsers as $user) {
                            echo '<option value="' . $user->getId() . '" ';
                            if ($user->getId() == $article->getOwner()->getId()) {
                                echo "selected";
                            }
                            echo '>' . $user->getName() . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group required">
                    <label class="control-label">Inhalt *</label>
                    <textarea class="form-control" name="content" rows="10"><?= htmlspecialchars($article->getContent()) ?></textarea>
                    <?php
                    if (isset($article->getErrors()['content'])) {
                        echo '<p class="error">' . $article->getErrors()['content'] . '</p>';
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="update">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
</body>
</html>