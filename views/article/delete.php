<?php

include "../helper/sessionHelper.php";
include "../helper/head.php";
include "../../models/Article.php";
include "../../models/User.php";
include "../helper/navbar.php";

?>

<!DOCTYPE html>
<html lang="de">

<?php

$article = Article::get($_GET['id']);
if ($article == null || $article->getOwner()->getId() != $_SESSION['user']) {
    header("Location: index.php");
    exit();
}

if (isset($_POST['delete'])) {
    Article::delete($_POST['id']);
    header("Location: index.php");
    exit();
}
?>

<body>

<div class="container">
    <h2>Beitrag löschen</h2>

    <form class="form-horizontal" action="delete.php?id=<?= $_GET['id']; ?>" method="post">
        <input type="hidden" name="id" value="<?= $_GET['id']; ?>"/>
        <p class="alert alert-error">Wollen Sie den Beitrag wirklich löschen?</p>
        <div class="form-actions">
            <button type="submit" class="btn btn-danger" name="delete">Löschen</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
</body>
</html>