<?php

include "../helper/sessionHelper.php";
include "../helper/head.php";
include "../../models/Article.php";
include "../../models/User.php";
include "../helper/navbar.php";

?>

<!DOCTYPE html>
<html lang="de">

<body>

<div class="container">
    <div class="row">
        <h2>Beiträge</h2>
    </div>
    <div class="row">
        <p>
            <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
        </p>

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Titel</th>
                <th>Inhalt</th>
                <th>Besitzer</th>
                <th>Freigabedatum</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            <?php
            $allArticles = getAllArticlesToDisplay();
            $specificUser = false;
            if(isset($_GET['u_id'])){
                $specificUser = true;
            }
            foreach ($allArticles as $article) {

                $userIsLoggedIn = false;
                if($_SESSION['user'] == $article->getOwner()->getId()){
                    $userIsLoggedIn = true;
                }

                echo '<tr>';
                echo '<td>' . $article->getTitle() . '</td>';
                echo '<td>' . limitArticleLengthToDisplay($article) . '</td>';
                echo '<td>' . $article->getOwner()->getName() . '</td>';
                echo '<td>' . $article->getDate() . '</td>';

                echo '<td><a class="btn btn-info" href="view.php?id='.$article->getId();
                if($specificUser){
                    echo "&u_id=".$_GET['u_id'];
                }
                echo'">
                    <span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;
                    <a class="btn btn-primary" ';

                if ($userIsLoggedIn) {
                    echo 'href="update.php?id=' . $article->getId() . '">';
                } else {
                    echo 'disabled="disabled">';
                }

                echo '<span class="glyphicon glyphicon-pencil"></span></a>&nbsp;
                    <a class="btn btn-danger"';

                if ($userIsLoggedIn) {
                    echo 'href = "delete.php?id=' . $article->getId() . '" >';
                } else {
                    echo 'disabled="disabled">';
                }

                echo '<span class="glyphicon glyphicon-remove"></span></a>
                </td>
                </tr>';
            }
            ?>

            </tbody>
        </table>
    </div>
</div> <!-- /container -->
</body>
</html>