<?php

include "../helper/sessionHelper.php";
include "../helper/head.php";
include "../../models/User.php";
include "../../models/Article.php";
include "../helper/navbar.php";

?>

<!DOCTYPE html>
<html lang="de">

<?php

$article = new Article("", "", "", "", "");
if (isset($_POST["newArticle"])) {
    $article = new Article(0, $_POST["title"], $_POST["content"], $_POST["releasedate"], User::get($_SESSION["user"]));
    if ($article->validate()) {
        $article->save();
        header("Location: view.php?id=" . $article->getId());
        exit();
    }
}
?>

<body>

<div class="container">
    <div class="row">
        <h2>Beitrag erstellen</h2>
    </div>

    <form class="form-horizontal" action="create.php" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required">
                    <label class="control-label">Titel *</label>
                    <input type="text" class="form-control" name="title" maxlength="45"
                           value="<?=htmlspecialchars($article->getTitle()) ?>">
                </div>
                <?php
                if (isset($article->getErrors()['title'])) {
                    echo '<p class="error">' . $article->getErrors()['title'] . '</p>';
                }
                ?>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required">
                    <label class="control-label">Freigabedatum *</label>
                    <input type="date" class="form-control" name="releasedate"
                           value="<?=htmlspecialchars($article->getDate()) ?>">
                    <?php
                    if (isset($article->getErrors()['date'])) {
                        echo '<p class="error">' . $article->getErrors()['date'] . '</p>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="form-group required">
                    <label class="control-label">Besitzer</label>
                    <input type="text" class="form-control" name="owner"
                           value="<?php echo User::get($_SESSION['user'])->getName(); ?>" readonly>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group required">
                    <label class="control-label">Inhalt *</label>
                    <textarea class="form-control" name="content" rows="10" ><?=htmlspecialchars($article->getContent()) ?></textarea>
                    <?php
                    if (isset($article->getErrors()['content'])) {
                        echo '<p class="error">' . $article->getErrors()['content'] . '</p>';
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" name="newArticle" class="btn btn-success">Erstellen</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
</body>
</html>