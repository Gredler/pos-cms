<?php

include "../helper/sessionHelper.php";
include "../helper/head.php";
include "../../models/Article.php";
include "../../models/User.php";
include "../helper/navbar.php";

?>

<!DOCTYPE html>
<html lang="de">

<?php

$article = Article::get($_GET['id']);
if ($article == null) {
    header("Location: index.php");
    exit();
}
?>

<body>

<div class="container">
    <h2>Beitrag anzeigen</h2>

    <p>

        <?php
        $isOwnerLoggedIn = false;
        if ($_SESSION['user'] == $article->getOwner()->getId()) {
            $isOwnerLoggedIn = true;
        }
        ?>

        <a class="btn btn-primary"
        <?php
        if ($isOwnerLoggedIn) {
            echo 'href="update.php?id=' . $_GET['id'];
        } else {
            echo 'disabled="disabled';
        }
        echo '">Aktualisieren</a>';
        ?>
        <a class="btn btn-danger"
        <?php
        if ($isOwnerLoggedIn) {
            echo 'href="delete.php?id=' . $_GET['id'];
        } else {
            echo 'disabled="disabled';
        }
        echo '">Löschen</a>&nbsp;';

        echo '<a class="btn btn-default" href="';
        if(isset($_GET['u_id'])){
            echo 'index.php?u_id='.$_GET['u_id'];
        } else {
            echo 'index.php';
        }

        echo '">Zurück</a>';
        ?>
    </p>

    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>Titel</th>
            <td><?= $article->getTitle() ?></td>
        </tr>
        <tr>
            <th>Freigabedatum</th>
            <td><?= $article->getDate() ?></td>
        </tr>
        <tr>
            <th>Besitzer</th>
            <td><?= $article->getOwner()->getName() ?></td>
        </tr>
        <tr>
            <th>Inhalt</th>
            <td><?= $article->getContent() ?></td>
        </tr>
        </tbody>
    </table>
</div> <!-- /container -->
</body>
</html>