<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 06.01.2018
 * Time: 14:28
 */

session_start();

include "../helper/navbar.php";
include "../helper/head.php";
include "../../models/Article.php";
include "../../models/User.php";

?>

<!DOCTYPE html>
<html lang="de">

<?php

$article = Article::get($_GET['id']);
if ($article == null) {
    header("Location: /pos-cms/index.php");
    exit();
}
?>

<body>

<div class="container">
    <div class="row">

        <?php
        echo '<div class="col-md-12">';
        echo '<h1>' . $article->getTitle() . '</h1>';
        echo '<small>Verantwortlich: ' . $article->getOwner()->getName() . '</small><br/>';
        echo '<small>Freigabedatum: ' . $article->getDate() . '</small>';
        echo '<p><br/>' . $article->getContent() . '</p>';
        echo '<a class="btn btn-default" href="/pos-cms/index.php">Zurück</a>';
        echo '</div>';
        ?>

    </div>

    <hr>

    <footer>
        <p>&copy; 2017 Company, Inc.</p>
    </footer>
</div> <!-- /container -->
