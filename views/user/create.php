<?php

include "../helper/sessionHelper.php";
include "../helper/head.php";
include "../../models/User.php";
include "../../models/Article.php";
include "../helper/navbar.php";

?>

<!DOCTYPE html>
<html lang="de">

<?php

$user = new User("", "", "", "");

if (isset($_POST["newUser"])) {
    $user = new User(0, $_POST['uname'], password_hash($_POST['pw1'], PASSWORD_DEFAULT), $_POST['email']);
    $pconfirm = $user->confirmPassword($_POST['pw1'], $_POST['pw2']);
    if ($user->validate() && $pconfirm) {
        $user->save();
        header("Location: view.php?id=" . $user->getId());
        exit();
    }
}
?>

<body>

<div class="container">
    <div class="row">
        <h2>Benutzer erstellen</h2>
    </div>

    <form class="form-horizontal" action="create.php" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required">
                    <label class="control-label">Benutzername *</label>
                    <input type="text" class="form-control" name="uname" maxlength="45" value="<?=htmlspecialchars($user->getName()) ?>">
                    <?php
                    if (isset($user->getErrors()['uname'])) {
                        echo '<p class="error">' . $user->getErrors()['uname'] . '</p>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <div class="form-group required">
                    <label class="control-label">E-Mail *</label>
                    <input type="email" class="form-control" name="email" value="<?=htmlspecialchars($user->getEmail()) ?>">
                    <?php
                    if (isset($user->getErrors()['email'])) {
                        echo '<p class="error">' . $user->getErrors()['email'] . '</p>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row">
            <div class="col-md-5 form-group required">
                <label class="control-label">Passwort *</label>
                <input type="password" class="form-control" name="pw1" value="">
                <?php
                if (isset($user->getErrors()['pwconfirm'])) {
                    echo '<p class="error">' . $user->getErrors()['pwconfirm'] . '</p>';
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 form-group required">
                <label class="control-label">Passwort Wiederholen *</label>
                <input type="password" class="form-control" name="pw2" value="">
            </div>
        </div>

        <div class="form-group">
            <button type="submit" name="newUser" class="btn btn-success">Erstellen</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
</body>
</html>