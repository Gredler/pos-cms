<?php

include "../helper/sessionHelper.php";
include "../helper/head.php";
include "../../models/Article.php";
include "../../models/User.php";
include "../helper/navbar.php";

?>

<!DOCTYPE html>
<html lang="de">

<?php

$user = User::get($_GET['id']);
if ($user == null || $user->getId() != $_SESSION['user']) {
    header("Location: index.php");
    exit();
}

if (isset($_POST['delete'])) {
    User::delete($_POST['id']);
}
?>

<body>

<div class="container">
    <h2>Benutzer löschen</h2>

    <form class="form-horizontal" action="delete.php?id=<?= $_GET['id']; ?>" method="post">
        <input type="hidden" name="id" value="<?= $_GET['id']; ?>"/>
        <p class="alert alert-error">Wollen Sie den Benutzer wirklich löschen?</p>
        <div class="form-actions">
            <button type="submit" class="btn btn-danger" name="delete">Löschen</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
</body>
</html>