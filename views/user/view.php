<?php

include "../helper/sessionHelper.php";
include "../helper/head.php";
include "../../models/User.php";
include "../helper/navbar.php";

?>

<!DOCTYPE html>
<html lang="de">

<?php

$user = User::get($_GET['id']);
if ($user == null) {
    header("Location: index.php");
    exit();
}
?>

<body>

<div class="container">
    <h2>Benutzer anzeigen</h2>

    <p>
        <a class="btn btn-primary"
        <?php
        if ($_SESSION['user'] == $user->getId()) {
            echo 'href="update.php?id=' . $_GET['id'];
        } else {
            echo 'disabled="disabled';
        }
        echo '">Bearbeiten</a>';
        ?>
        <a class="btn btn-danger"
        <?php
        if ($_SESSION['user'] == $user->getId()) {
            echo 'href="delete.php?id=' . $_GET['id'];
        } else {
            echo 'disabled="disabled';
        }
        echo '">Löschen</a>&nbsp;';
        echo '<a class="btn btn-default" href="index.php">Zurück</a>';
        ?>
    </p>

    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>Benutzername</th>
            <td><?= $user->getName() ?></td>
        </tr>
        <tr>
            <th>E-Mail</th>
            <td><?= $user->getEmail() ?></td>
        </tr>
        </tbody>
    </table>
</div> <!-- /container -->
</body>
</html>