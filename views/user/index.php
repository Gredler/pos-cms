<?php

include "../helper/sessionHelper.php";
include "../helper/head.php";
include "../../models/Article.php";
include "../../models/User.php";
include "../helper/navbar.php";

?>

<!DOCTYPE html>
<html lang="de">

<body>

<div class="container">
    <div class="row">
        <h2>Benutzer</h2>
    </div>
    <div class="row">
        <p>
            <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
        </p>

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Name</th>
                <th>E-Mail</th>
                <th>Optionen</th>
            </tr>
            </thead>
            <tbody>

            <?php

            $allUsers = User::getAll();
            foreach ($allUsers as $user) {
                $userIsLoggedIn = false;
                if($_SESSION['user'] == $user->getId()){
                    $userIsLoggedIn = true;
                }
                echo '<tr>';
                echo '<td>' . $user->getName() . '</td>';
                echo '<td>' . $user->getEmail() . '</td>';

                echo '<td><a class="btn btn-info" href="view.php?id=' . $user->getId() . '">
                    <span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;
                    <a class="btn btn-primary" ';

                if ($userIsLoggedIn) {
                    echo 'href="update.php?id=' . $user->getId() . '">';
                } else {
                    echo 'disabled="disabled">';
                }

                echo '<span class="glyphicon glyphicon-pencil"></span></a>&nbsp;
                    <a class="btn btn-danger"';

                if ($userIsLoggedIn) {
                    echo 'href = "delete.php?id=' . $user->getId() . '" >';
                } else {
                    echo 'disabled="disabled">';
                }

                echo '<span class="glyphicon glyphicon-remove"></span></a>&nbsp;&nbsp;';

                echo '<a class="btn btn-success" href="/pos-cms/views/article/index.php?u_id=' . $user->getId() . '">
                    <span class="glyphicon glyphicon-folder-open"></span></a>&nbsp;';

                echo '</td>';
                echo '</tr>';
            }

            ?>

            </tbody>
        </table>
    </div>
</div> <!-- /container -->
</body>
</html>