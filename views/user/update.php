<?php

include "../helper/sessionHelper.php";
include "../helper/head.php";
include "../../models/User.php";
include "../helper/navbar.php";

?>

<!DOCTYPE html>
<html lang="de">
<?php

$user = User::get($_GET['id']);
if ($user == null) {
    header("Location: index.php");
    exit();
}

if (isset($_POST['update'])) {
    $name = trim($_POST['uname']) == "" ? $user->getName() : $_POST['uname'];
    $email = trim($_POST['email']) == "" ? $user->getEmail() : $_POST['email'];
    $pwold = $_POST['pwold'] == "" ? null : $_POST['pwold'];
    $pwnew1 = $_POST['pwnew1'] == "" ? null : $_POST['pwnew1'];
    $pwnew2 = $_POST['pwnew2'] == "" ? null : $_POST['pwnew2'];

    if ($user->checkPassword($pwold)) {
        $user->setName($name);
        $user->setEmail($email);

        $newPasswordRepetitionEqual = true;
        if ($pwnew1 != null || $pwnew2 != null) {
            if ($user->confirmPassword($pwnew1, $pwnew2)) {
                $user->setPassword(password_hash($pwnew1, PASSWORD_DEFAULT));
            } else {
                $newPasswordRepetitionEqual = false;
            }
        }

        if ($user->validate() && $newPasswordRepetitionEqual) {
            $user->save();
            header("Location: view.php?id=" . $user->getId());
            exit();
        }
    }
}
?>

<body>

<div class="container">
    <div class="row">
        <h2>Profil bearbeiten</h2>
    </div>

    <form class="form-horizontal" action="update.php?id=<?= $_GET['id'] ?>" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required">
                    <label class="control-label">Benutzername *</label>
                    <input type="text" class="form-control" name="uname" maxlength="50"
                           value="<?= $user->getName() ?>">
                    <?php
                    if (isset($user->getErrors()['uname'])) {
                        echo '<p class="error">' . $user->getErrors()['uname'] . '</p>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <div class="form-group required">
                    <label class="control-label">E-Mail *</label>
                    <input type="email" class="form-control" name="email" maxlength="100"
                           value="<?= $user->getEmail() ?>">
                    <?php
                    if (isset($user->getErrors()['email'])) {
                        echo '<p class="error">' . $user->getErrors()['email'] . '</p>';
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 form-group required">
                <label class="control-label">Altes Passwort *</label>
                <input type="password" class="form-control" name="pwold" value="">
                <?php
                if (isset($user->getErrors()['password'])) {
                    echo '<p class="error">' . $user->getErrors()['password'] . '</p>';
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 form-group">
                <label class="control-label">Neues Passwort</label>
                <input type="password" class="form-control" name="pwnew1" value="">
                <?php
                if (isset($user->getErrors()['pwconfirm'])) {
                    echo '<p class="error">' . $user->getErrors()['pwconfirm'] . '</p>';
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 form-group">
                <label class="control-label">Passwort wiederholen</label>
                <input type="password" class="form-control" name="pwnew2" value="">
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="update">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
</body>
</html>